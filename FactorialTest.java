package com.ust.junitExample;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class FactorialTest {
	
	Factorial fact=null;
	@Before
	public void setUp() {
		fact=new Factorial();
		System.out.println("Before Will be executed for each Test Method");
	}

	@Test
	public void findFactorialForZeroThenReturnOne(){
		System.out.println("Method 1");
		//Factorial fact = new Factorial();
		int result = fact.findFactorial(0);
		
		assertEquals(1, result);
	}
	@Test
	public void findFactorialForOneThenReturnOne() {
		System.out.println("Method 2");
		//Factorial fact = new Factorial();
		int result = fact.findFactorial(1);
		assertEquals(1, result);
	}
	@Test
	public void findFactorialForValidPositiveInteger() {
		System.out.println("Method 3");
		//Factorial fact = new Factorial();
		int result = fact.findFactorial(5);
		assertEquals(120, result);
		assertEquals(720, fact.findFactorial(6));
	}
	@Test
	public void findFactorialForInValidNegativeInteger() {
		System.out.println("Method 4");
		//Factorial fact = new Factorial();
		int result = fact.findFactorial(-5);
		assertEquals(-1, result);
		
	}
	@After
	public void teardown() {
		fact =null;
		System.out.println("After will be executed for each test method!");
	}
	
	
}
